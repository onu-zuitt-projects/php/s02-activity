<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>S02: Repetition Control Structures and Array Manipulation</title>
	</head>
	<body>

		<h2>Divisible of Five</h2>
		<p><?php printDivisibleOfFive(); ?></p>

		<h2>Array Manipulation</h2>
		<!-- accept a name and add to students array -->
		<p><?php array_push($students, "obi", "ada", "chioma", "obi", "ada", "obi"); ?></p>

		<!-- print the names added so far -->
		<p><?php print_r($students); ?></p>

		<!-- count the number of students -->
		<p><?php print_r(array_count_values($students)); ?>

		<!-- add another student to the array -->
		<p><?php array_push($students, "ugochi"); ?></p>

		<!-- print the updated students array-->
		<p><?php print_r($students); ?></p>

		<!-- print the updated count of student array  -->
		<p><?php print_r(array_count_values($students)); ?>

		<!-- remove the first student -->
		<p><?php array_shift($students); ?></p>

		<!-- print the updated students array to reflect removed element-->
		<p><?php print_r($students); ?></p>

		<!-- print the updated count of student array -->
		<p><?php print_r(array_count_values($students)); ?>
	</body>
</html>